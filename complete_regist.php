<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign in Successful</title>
</head>
<style>
    html{
        font-size: 15px;
    }
    fieldset{
        
        width: max(100px, 15vw);
        max-height: 100%;
        border: 2px solid #000000;
        margin: 0 auto;
    }
    a{
        padding: 0 25px;
    }
</style>
<body>
    <fieldset>
        <p>Bạn đã dăng ký thành công sinh viên</p>
        <div>
            <a href="list.php">Quay lại danh sách sinh viên</a>
        </div>
    </fieldset>
</body>
</html>
