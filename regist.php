<?php
    include 'connect.php';
    session_start();


    if (!file_exists("Image")) {
        mkdir("Image");
    }
    $gender = array(1 => 'Nam', 0 => 'Nữ');
    $faculty = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
    $err = array();
    $err["name"] = "";
    $err["gender"] = "";
    $err["faculty"] = "";
    $err["birthday"] = "";
    $err["loadImage"] = "";
    
    if (isset($_POST['submit'])) {
        $_SESSION["name"] = "";
        $_SESSION["gender"] = "";
        $_SESSION["genderKey"] = "";
        $_SESSION["faculty"] = $faculty[$_POST["faculty"]];
        $_SESSION["facultyKey"] = $_POST["faculty"];
        $_SESSION["birthday"] = "";
        $_SESSION["address"] = "";
        $_SESSION["loadImage"] = "";
        $_SESSION["nameImage"] = "";
        $_SESSION["submit_again"] = False;

        if (empty($_POST["name"])) {
            $err["name"] = "<div style='color:red' >Hãy nhập tên.</div>";
        }
        else {
            $_SESSION["name"] = $_POST["name"];
        }
        
        if (isset($_POST["gender"]) == FALSE) {
            $err["gender"] = "<div style='color:red' >Hãy chọn giới tính.</div>";
        }
        else {
            $_SESSION["gender"] =  $gender[$_POST["gender"]];
            $_SESSION["genderKey"] = $_POST["gender"];
        }

        if ($_SESSION["faculty"] === "") {
            $err["faculty"] = "<div style='color:red' >Hãy chọn phân khoa.</div>";
        }

        if (empty($_POST["birthday"])) {
            $err["birthday"] = "<div style='color:red' >Hãy chọn ngày sinh.</div>";
        }
        else if (!empty($_POST["birthday"]) && !isValidateDate($_POST["birthday"], 'd/m/Y')) {
            $err["birthday"] = "<div style='color:red' >Hãy nhập ngày sinh đúng định dạng.</div>";
        }
        else if (!empty($_POST["birthday"]) && isValidateDate($_POST["birthday"], 'd/m/Y')) {
            $_SESSION["birthday"] = $_POST["birthday"];
        }
            
        $_SESSION["address"] = $_POST["address"];

        $target_dir = "Image/";
        $file_name = basename($_FILES['loadImage']['name']);
        $target_file = $target_dir.$file_name;

        $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
        $file_type_allow = array('png', 'jpg', 'jpeg', '');

        $temp = explode(".", $file_name);
        if ($temp[0] != "") {
            $new_name = $temp[0]."_".date("YmdHis").".".end($temp);
            $new_target_file = $target_dir.$new_name;
            $_SESSION["nameImage"] = $temp[0];
        } else {
            // $new_name = "";
            $new_target_file = "";
            $_SESSION["nameImage"] = $temp[0];
        }
        
        if (isValidImage($target_file, $file_type, $file_type_allow) == true) {
            move_uploaded_file($_FILES['loadImage']['tmp_name'], $new_target_file);
            $_SESSION["loadImage"] = $new_target_file;
        } else {
            $err["loadImage"] = "<div style='color:red' >Chỉ upload ảnh và ảnh có định dạng png, jpg và jpeg.</div>";
        }

        if (($_SESSION["name"] != "" && $_SESSION["gender"] != "" && $_SESSION["faculty"] != "" && $_SESSION["birthday"] != "" && isValidImage($target_file, $file_type, $file_type_allow) == true)) {
            $_SESSION["submit_again"] = True;
            header('Location: upload.php');
        }  
    }

    function isValidateDate($date, $format = 'd/m/Y') {
        $dt = DateTime::createFromFormat($format, $date);
        return $dt && $dt->format($format) === $date;
    }

    function isValidImage($target_file, $file_type, $file_type_allow) {
        if (!in_array($file_type, $file_type_allow)) {
            return false;
        } else {            
            return true;
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset='UTF-8'>
</head>
<style>
.fieldset{
    width: 450px;
    max-height: 800px; 
    margin: auto;
    padding-left: 15px;
    border: 2px solid steelblue;
    }
button {
  width: 125px;
  height: 43px;
  margin: 20px 5px 0 140px;
  background-color: green;
  color: white;
  border: 2px solid steelblue;
  border-radius: 10px;
  padding-top: 5px;
  text-align: center;
}
table {
  border-spacing: 10px;
}
.td {
  background-color: green;
  color: white;
  text-align: center;
  margin: 0 0 5px 0;
  padding: 3px 13px 10px 13px;
  border: 2px solid steelblue;
}
#input {
  width: 280px;
  height: 35.4px;
}
#gender {
  padding: 5px;
  font-size: 16px;
}
</style>
<body>
    <form method='post' action='regist.php' enctype='multipart/form-data'>
    <div class="fieldset">
        <?php
            echo $err["name"];
            echo $err["gender"];
            echo $err["faculty"];
            echo $err["birthday"];
            echo $err["loadImage"];
        ?>
        <table>
            <tr>
                <td class='td'><label>Họ và tên<span style='color:red;'>*</span></label></td>
                <td>
                    <?php 
                        echo "<input type='text' id='input' class='box' name='name' value='";
                        echo isset($_POST['name']) ? $_POST['name'] : '';
                        echo "'>"; 
                    ?>
                </td>         
                <tr>
                    <td class='td'><label>Giới tính<span style='color:red;'>*</span></label></td>
                    <td>
                        <?php
                            for ($i = count($gender)-1; $i >= 0; $i--) {
                                echo "<input type='radio' name='gender' class='gender' value='" . $i . "'";
                                echo (isset($_POST['gender']) && $_POST['gender'] == $i) ? " checked " : "";
                                echo "/>" . $gender[$i];
                            }
                        ?>
                    </td>
                </tr>    
                <tr>
                    <td class='td'><label>Phân khoa<span style='color:red;'>*</span></label></td>
                    <td>
                        <select class='box' name='faculty' id="input">
                            <?php
                                foreach ($faculty as $key => $value) {
                                    echo "<option";
                                    echo (isset($_POST['faculty']) && $_POST['faculty'] == $key) ? " selected " : "";
                                    echo " value='" . $key . "'>" . $value . "</option>";
                                }
                            ?>  
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class='td'><label>Ngày sinh<span style='color:red;'>*</span></label></td>
                    <td >
                        <?php
                            echo "<input type='text' class='box' id='input' name='birthday' value='";
                            echo isset($_POST['birthday']) ? $_POST['birthday'] : '';
                            echo "'>";
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class='td'><label>Địa chỉ</label></td>
                    <td>
                        <?php
                            echo "<input type='text' id = 'input' class='box' name='address' value='";
                            echo isset($_POST['address']) ? $_POST['address'] : '';
                            echo "'>";
                        ?>
                    </td>
                </tr>
            </table>

        <div class="content">
            <label id='image'>Hình ảnh</label>
            <form id="form_upload" method="post" enctype="multipart/form-data">
                <input type="file" name="loadImage">
            </form>
        </div>
        <button name='submit' type='submit'>Đăng ký</button>
    </div>
    </form>
</body>
</html>


        
