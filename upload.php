<?php
include 'connect.php';
session_start();

$ds = $_SESSION["birthday"];
$datetime = DateTime::createFromFormat('d/m/Y', $ds);
$birthday = $datetime->format('Y-m-d g:i:s');

$symbol = "/";
if ($_SESSION["nameImage"] != "") {
    $image_path = $symbol.$_SESSION["loadImage"];
} else {
    $image_path = "";
}
if (isset($_POST['submit'])) {
    $gender = $_SESSION["genderKey"];
    $sql = "INSERT INTO Student (name, gender, faculty, birthday, address, avartar) 
                VALUES ('" . $_SESSION["name"] . "', $gender, '" . $_SESSION["facultyKey"] . "', '" . $birthday . "', '" . $_SESSION["address"] . "', '" . $image_path . "')";
    $id = mysqli_insert_id($con);
    $con->query('ALTER TABLE `Student` AUTO_INCREMENT = ' . $id);
    mysqli_query($con, $sql);
    header('Location: complete_regist.php');
}
?>

<head>
    <meta charset='UTF-8'>
</head>
<style>
fieldset {
  width: 450px;
  height: 400px;
  margin: auto;
  padding-left: 15px;
  border: 2px solid steelblue;
}
form {
  margin-left: 5px;
}

table {
  border-spacing: 10px;
}

button {
  width: 125px;
  height: 43px;
  margin-top: 20px;
  background-color: green;
  color: white;
  border: 2px solid steelblue;
  margin-left: 140px;
  padding-top: 5px;
  text-align: center;
}

.td {
  background-color: green;
  color: white;
  text-align: center;
  padding: 3px 13px 5px 13px;
  border: 2px solid steelblue;
}
</style>
<body>
    <form method='post' action='upload.php'>
    <fieldset>
        <form>
            <table>
                <tr>
                    <td class='td'><label>Họ và tên</label></td>
                    <td><label><?php echo $_SESSION["name"]; ?></label></td>
                </tr>
                <tr>
                    <td class='td'><label>Giới tính</label></td>
                    <td><label><?php echo $_SESSION["gender"]; ?></label></td>
                </tr>
                <tr>
                    <td class='td'><label>Phân khoa</label></td>
                    <td><label><?php echo $_SESSION["faculty"]; ?></label></td>
                </tr>
                <tr>
                    <td class='td'><label>Ngày sinh</label></td>
                    <td><label><?php echo $_SESSION["birthday"]; ?></label></td>
                </tr>
                <tr>
                    <td class='td'><label>Địa chỉ</label></td>
                    <td><label><?php echo $_SESSION["address"]; ?></label></td>
                </tr>
                <tr>
                    <td class='td'><label>Hình ảnh</label></td>
                    <td><img src= <?php echo $_SESSION["loadImage"]; ?> height=70 width=100></td>
                </tr>
            </table>
            <button name='submit' type='submit'>Xác nhận</button>
        </form>
    </fieldset>
</body>

