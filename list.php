<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<style>
.fieldset{
    width: 500px;
    height: 430px;
    margin: auto;
    padding-left: 15px;
    border: 1px solid gray;"
}
.box{
    border: 2px solid steelblue;
    line-height: 17px;
    padding: 3px 0px 5px 0px;
}
.add{
    width: 70px;
    height: 20px;
    padding: 0 3px 1px;
    margin: 20px 0 0 380px;
    text-align: center;
    border: 3px solid #287115; 
    background-color: green;
    color: #ffffff;
}
button{
    width: 100px;
    height: 30px;
    margin: 20px 70px 25px 0;;
    background-color: green;
    color: white;
    border: 2px solid #287115;
    text-align: center;
}
td {
    text-align: left;
    padding: 3px;
}

th {
    font-weight: normal;
    text-align: left;
    padding: 3px;
}
.action {
    color: white;
    display: inline-block;
    font-size: 14px;
    padding: 2.5px 12px;
    background-color: rgb(125, 174, 216);
    border: 2px solid steelblue;
    margin-right: 8px;
}

</style>
<body>
    <form method='post' action='list.php' enctype='multipart/form-data'>
    <div class="fieldset" >
        <?php
            $phankhoa = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");

            if (isset($_POST['search'])) {
                session_start();
                $h = $_POST['tukhoa'];
            }
        ?>
        <div class='field'>
            <table>
                <tr>
                    <td class='td'><label>Khoa</label></td>
                    <td><select class='box' id='input' name='phankhoa'>
                    <?php
                        foreach ($phankhoa as $key => $value) {
                            echo "<option";
                            echo (isset($_POST['phankhoa']) && $_POST['phankhoa'] == $key) ? " selected " : "";
                            echo " value='" . $key . "'>" . $value . "</option>";
                        }
                    ?>
                    </select>
                    </td>
                </tr>
                <tr>
                    <td class='td'><label>Từ khóa</label></td>
                    <td><input type='text' class='box' id='key' name='tukhoa' value= <?php echo @$h; ?>>
                    <?php 
                        isset($_POST['tukhoa']) ? $_POST['tukhoa'] : '';
                    ?>
                    </td>
                </tr>
            </table>
                <button name='reset' type='submit'>Xóa</button>
                <button name='search' type='submit'>Tìm kiếm</button>
        </div>
            
        <div class ='result'>
            <p>Số sinh viên tìm thấy: XXX</p>
            <span class="add" href="regist.php">Thêm </span>
        </div>

        <div class='list_student'>
            <table>
                <colgroup>
                    <col span="1" style="width: 5%;">
                    <col span="1" style="width: 28%;">
                    <col span="1" style="width: 37%;">
                    <col span="1" style="width: 30%;">
                </colgroup>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Nguyễn Văn A</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <span class="action">Xóa</span>
                        <span class="action">Sửa</span>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Trần Thị B</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <span class="action">Xóa</span>
                        <span class="action">Sửa</span>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Nguyễn Hoàng C</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <span class="action">Xóa</span>
                        <span class="action">Sửa</span>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Đinh Quang D</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <span class="action">Xóa</span>
                        <span class="action">Sửa</span>
                    </td>
                </tr>
            </table>
        </div>           
    </div>
</body>
<script>
    $(document).ready(function(){
        $("button[name='reset']").click(function(){
            $("#input").val('');
            $("#key").val('');
        });
    });
</script>
</html>
